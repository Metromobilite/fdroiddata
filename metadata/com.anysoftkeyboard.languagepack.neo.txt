Categories:Writing
License:Apache2,GPLv3+
Web Site:http://neo-layout.org
Source Code:https://github.com/kertase/neo_anysoftkeyboard
Issue Tracker:https://github.com/kertase/neo_anysoftkeyboard/issues

Name:AnySoftKeyboard: NEO2
Auto Name:neo2 for AnySoftKeyboard
Summary:Neo2 Keyboard Layout for ASK
Description:
Neo2 Keyboard Layout for ASK.

For more information about Neo2 visit [http://neo-layout.org Neo-layout]

Install [[com.menny.android.anysoftkeyboard]] first, then select the desired
layout from AnySoftKeyboard's Settings->Keyboards menu.
.

Repo Type:git
Repo:https://github.com/kertase/neo_anysoftkeyboard

Build:1.1,4
    commit=337f3a6
    target=android-23

Build:1.2,5
    commit=d3263df
    target=android-23

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.2
Current Version Code:5
